var express = require('express');
var router = express.Router();
const mysql = require('mysql')

/* GET home page. */
router.get('/', function(req, res, next) {
	const con = DataBaseConnection.getConnection()
	con.query('select * from hellotable0', function (err, rows, fields) {
		const queryStr = rows[0].name
		res.render('index', { title: 'Express ' + queryStr + ' by yanwosky4'})
	})
	con.end()
	// res.render('index', { title: 'Express' });
});

class DataBaseConnection {
	static getConnection () {
		const connection = mysql.createConnection({
			host: 'localhost',
			user: 'root',
			password: 'root',
			database : 'test'
		})
		connection.connect()
		return connection
	}
}

module.exports = router;
